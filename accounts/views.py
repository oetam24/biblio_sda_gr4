from django.contrib import messages
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.views.generic import CreateView

from .forms import SubmittableAuthenticationForm, SignUpForm


#

class SubmittableLoginView(LoginView):
    title = 'Login'
    form_class = SubmittableAuthenticationForm
    template_name = 'login_form.html'
    success_url = reverse_lazy('index')


class SuccessMessagedLogoutView(LogoutView):
    def get_next_page(self):
        result = super().get_next_page()
        messages.success(self.request, 'Successfully logged out!')
        return result


class SignUpView(CreateView):
    title = 'Sign Up'
    success_message = 'Successfully signed up!'
    template_name = 'login_form.html'
    form_class = SignUpForm
    success_url = reverse_lazy('index')
