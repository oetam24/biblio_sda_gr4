from django.forms import ModelForm

from .models import Book


class AddBookForm(ModelForm):
    class Meta:
        model = Book
        fields = '__all__'


class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = '__all__'
