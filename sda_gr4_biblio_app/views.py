from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView
#  Tylko zalogowani uzytkownicy moga dodawac ksiazki
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Book

from .forms import AddBookForm
from .forms import BookForm

class IndexView(ListView):
    template_name = 'base.html'
    model = Book


class BooksListView(ListView):
    template_name = 'books_list.html'
    model = Book


class AddBookView(LoginRequiredMixin, CreateView):
    template_name = 'book_new.html'
    form_class = AddBookForm
    success_url = reverse_lazy('index')

# Użytkownik ma możliwość edytowania ksiażki

def details_book(request,id):
    book = get_object_or_404(Book, pk=id)
    form = BookForm(request.POST or None, request.FILES or None, instance=book)

    if form.is_valid():
        form.save()

    return render(request, 'details.html', {'form': form})

# Użytkownik ma możliwość wyświetlania szczegółów ksiażki

def book_info(request,id):
    book = get_object_or_404(Book, pk=id)
    return render(request, 'book_info.html', {'book': book})

