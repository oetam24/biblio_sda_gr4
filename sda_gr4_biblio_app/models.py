from django.db import models


# dodany blank=false zeby nie mozna bylo dodac rekordu bez tytulu i unique = true
# zeby nie mozna bylo stworzyc rekordu zawierajacego to samo pole

class Room(models.Model):
    room_name = models.CharField(max_length=64, blank=False, unique=True)
    description_room = models.TextField(default="")

    def __str__(self):
        return self.room_name

class Rack(models.Model):
    rack_name = models.CharField(max_length=64, blank=False, unique=True)
    rack_location = models.TextField(default="")
    rack_description = models.TextField(default="")
    room = models.ForeignKey(Room, on_delete=models.CASCADE, null=True)
    numbers_of_shelves_in_rack = models.PositiveSmallIntegerField(default=5)

    def __str__(self):
        return self.rack_name

class Shelf(models.Model):
    shelf_name = models.CharField(max_length=64, blank=False, unique=True, null=True)
    number_number = models.PositiveSmallIntegerField(default=5)
    shelf_place = models.ForeignKey(Rack, on_delete=models.CASCADE, null=True)

class Book(models.Model):

    title = models.CharField(max_length=64, blank=False, unique=True)
    author = models.CharField(max_length=64)
    year = models.PositiveIntegerField(default=1900)
    ISBN = models.CharField(max_length=64, unique=True)
    Description = models.TextField(default="", blank=True)
    Rating = models.DecimalField(max_digits=4, decimal_places=1, default=0)
    Cover = models.ImageField(upload_to="okladki", null=True, blank=True)
    book_place = models.ForeignKey(Shelf, on_delete=models.CASCADE, null=True)
    # zmiana widoku wszystkich ksiazek z book object na Tytul + Rok

    def __str__(self):
        return self.title, ' (' + str(self.year) + ')'

