from django.contrib import admin
from django.urls import path

from sda_gr4_biblio_app.views import IndexView

from sda_gr4_biblio_app.views import BooksListView

from sda_gr4_biblio_app.views import AddBookView
from sda_gr4_biblio_app.views import details_book
from sda_gr4_biblio_app.views import book_info
from accounts.views import SubmittableLoginView, SuccessMessagedLogoutView, SignUpView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='index'),
    path('list/', BooksListView.as_view(), name='books_list'),
    path('book/add', AddBookView.as_view(), name='add_book'),
    path('accounts/login/', SubmittableLoginView.as_view(), name='login'),
    path('logout/', SuccessMessagedLogoutView.as_view(next_page='index'), name='logout'),
    path('sign-up/', SignUpView.as_view(), name='sign_up'),
    path('logout/', IndexView.as_view(), name='logout'),
    path('details/<int:id>/', details_book),
    path('book_info/<int:id>/', book_info),
]
